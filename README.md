# Titanic: Machine Learning from Disaster
Tip of the iceberg in ML world
# Submissions
## First submission
* Model: Logistic Regression
* Score: 0.75119
## Second submission
* Model: Gradient Boosting
* Score: 0.78468
# Data preprocessing
* Created features based on Parch and SibSp
* Dummy encoded Embedded column
